Metadata-Version: 2.1
Name: python-pskc
Version: 1.3
Summary: Python module for handling PSKC files
Home-page: https://arthurdejong.org/python-pskc/
Author: Arthur de Jong
Author-email: arthur@arthurdejong.org
License: LGPL
Description: Python module for handling PSKC files
        =====================================
        
        A Python module to handle Portable Symmetric Key Container (PSKC) files as
        defined in `RFC 6030 <https://tools.ietf.org/html/rfc6030>`_. PSKC files are
        used to transport and provision symmetric keys and key meta data (seed files)
        to different types of crypto modules, commonly one-time password systems or
        other authentication devices.
        
        This module can be used to extract keys from PSKC files for use in an OTP
        authentication system. The module can also be used for authoring PSKC files.
        
        This module should be able to handle most common PSKC files.
        
        https://arthurdejong.org/python-pskc/
        
        
        API
        ---
        
        The module provides a straightforward API that is mostly geared towards
        parsing existing PSKC files.
        
        Extracting key material from encrypted PSKC files is as simple as:
        
        >>> from pskc import PSKC
        >>> pskc = PSKC('tests/rfc6030/figure7.pskcxml')
        >>> pskc.encryption.derive_key('qwerty')
        >>> for key in pskc.keys:
        ...     print('%s %s' % (key.serial, str(key.secret.decode())))
        987654321 12345678901234567890
        
        Writing am encrypted PSKC file is as simple as:
        
        >>> pskc = PSKC()
        >>> key = pskc.add_key(
        ...     id='456', secret='987654321', manufacturer='Manufacturer',
        ...     algorithm = 'urn:ietf:params:xml:ns:keyprov:pskc:hotp')
        >>> pskc.encryption.setup_pbkdf2('passphrase')
        >>> pskc.write('output.pskcxml')
        
        The key object has a number of properties. See the pskc.key.Key documentation
        for details.
        
        
        Security considerations
        -----------------------
        
        This code handles private key material and is written in Python. No
        precautions have been taken to lock pages in memory to prevent swapping. Also
        no attempt is currently made to securely dispose of memory that may have held
        private key material.
        
        
        Copyright
        ---------
        
        Copyright (C) 2014-2024 Arthur de Jong
        
        This library is free software; you can redistribute it and/or
        modify it under the terms of the GNU Lesser General Public
        License as published by the Free Software Foundation; either
        version 2.1 of the License, or (at your option) any later version.
        
        This library is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
        Lesser General Public License for more details.
        
        You should have received a copy of the GNU Lesser General Public
        License along with this library; if not, write to the Free Software
        Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
        02110-1301 USA
        
Keywords: PSKC,RFC 6030,key container
Platform: UNKNOWN
Classifier: Development Status :: 5 - Production/Stable
Classifier: Intended Audience :: Developers
Classifier: Intended Audience :: Information Technology
Classifier: Intended Audience :: System Administrators
Classifier: License :: OSI Approved :: GNU Lesser General Public License v2 or later (LGPLv2+)
Classifier: Operating System :: OS Independent
Classifier: Programming Language :: Python
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.6
Classifier: Programming Language :: Python :: 3.7
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Programming Language :: Python :: 3.11
Classifier: Programming Language :: Python :: 3.12
Classifier: Programming Language :: Python :: Implementation :: PyPy
Classifier: Topic :: Security :: Cryptography
Classifier: Topic :: Software Development :: Libraries :: Python Modules
Classifier: Topic :: System :: Systems Administration :: Authentication/Directory
Classifier: Topic :: Text Processing :: Markup :: XML
Provides-Extra: defuse
Provides-Extra: lxml
Provides-Extra: signature
