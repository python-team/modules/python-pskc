python-pskc (1.3-2) unstable; urgency=medium

  * Team Upload
  * Set DPT as Maintainer per new Team Policy
  * Use dh-sequence-python3

  [ Arthur de Jong ]
  * Drop dependency on pkg-resources (closes: #1083684)

 -- Alexandre Detiste <tchet@debian.org>  Fri, 17 Jan 2025 13:41:08 +0100

python-pskc (1.3-1) unstable; urgency=medium

  * New upstream release:
    - typo fixes in documentation
    - have test suite not rely on current date/time (closes: #1078639)
    - update certificates in tests to support newer cryptography
  * Drop the pskc2csv.py example which is shipped as a command
  * Upgrade to standards-version 4.7.0 (no changes needed)

 -- Arthur de Jong <adejong@debian.org>  Sun, 08 Sep 2024 21:36:51 +0200

python-pskc (1.2-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on python3-all, python3-nose and
      python3-sphinx.
    + python-pskc-doc: Drop versioned constraint on python-pskc and python3-pskc
      in Replaces.
    + python-pskc-doc: Drop versioned constraint on python-pskc and python3-pskc
      in Breaks.

  [ Arthur de Jong ]
  * New upstream release:
    - sort namespace declarations alphabetically in generated XML
    - accept keys as bytearray values
    - spelling fixes in documentation
    - command-line utilities now support using - as stdin
    - test and build environment improvements
  * Upstream switched from nose to pytest (closes: #1018557)
  * Update debhelper compatibility level to 13
  * Update debian/watch version
  * Specify Rules-Requires-Root: no
  * Add basic debian/upstream/metadata file
  * Upgrade to standards-version 4.6.1 (no changes needed)
  * Update debian/copyright
  * Do not build Sphinx docs if nodocs is set

 -- Arthur de Jong <adejong@debian.org>  Tue, 13 Sep 2022 20:50:39 +0200

python-pskc (1.1-4) unstable; urgency=medium

  [ Arthur de Jong ]
  * Ensure lxml is installed for the tests

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 23:24:25 -0400

python-pskc (1.1-3) unstable; urgency=medium

  * Add missing dependency on pkg_resources
  * Remove Python 2 part from test suite

 -- Arthur de Jong <adejong@debian.org>  Wed, 28 Aug 2019 17:40:06 +0200

python-pskc (1.1-2) unstable; urgency=medium

  [ Arthur de Jong ]
  * Drop Python 2 support
  * Move sphinx dependency to python-pskc-doc
  * Make python-pskc-doc multi-arch foreign

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

 -- Arthur de Jong <adejong@debian.org>  Tue, 27 Aug 2019 14:37:13 +0200

python-pskc (1.1-1) unstable; urgency=medium

  [ Arthur de Jong ]
  * New upstream release:
    - add a remove_encryption() function
    - always write a 1.0 PSKC version, even when another version was read
    - correctly write a PSKC file with a global IV
    - correctly write a PSKC file without a MAC key
    - add a pskc2pskc script for converting a legacy PSKC file to a RFC 6030
      compliant version and for adding or removing encryption
    - add a csv2pskc script for generating a PSKC file from a CSV file
    - make all the scripts (pskc2csv, pskc2pskc and csv2pskc) entry points so
      they are available on package installation
  * Ship scripts in new pskc-utils package
  * Switch to using debian/master branch
  * Make signing key minimal (thanks lintian)
  * Upgrade to standards-version 4.3.0 (no changes needed)
  * Add a simple autopkgtest test suite

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs

 -- Arthur de Jong <adejong@debian.org>  Wed, 13 Feb 2019 22:45:14 +0100

python-pskc (1.0-1) unstable; urgency=medium

  * New upstream release:
    - fix a bug in writing passphrase encrypted PSKC files on Python3
    - fix a typo in the pin_max_failed_attempts attribute (the old name is
      available as a deprecated property)
    - switch from pycrypto to cryptography as provider for encryption
      functions because the latter is better supported
    - switch to using the PBKDF2 implementation from hashlib which requires
      Python 2.7.8 or newer
    - use defusedxml when available (python-pskc now supports both standard
      xml.etree and lxml with and without defusedxml)
    - support checking and generating embedded XML signatures (this requires
      the signxml library which is not required for any other operations)
      (note that signxml is currently not packaged in Debian)
    - add limited support for very old draft PSKC versions (it is speculated
      that this resembles the "Verisign PSKC format" that some applications
      produce)
    - support Camellia-CBC and KW-Camellia encryption algorithms
    - support any hashing algorithm available in Python
    - add a --secret-encoding option to pskc2csv to allow base64 encoded
      binary output
    - support naming the CSV column headers in pskc2csv
    - add a manual page for pskc2csv
    - a number of documentation, code style and test suite improvements
  * Drop patch to add missing file from tarball
  * Work around test suite issues
  * Switch to HTTPS URLs
  * Use Python3 version to build Sphinx documentation and use sphinx-build
  * Upgrade to standards-version 4.1.3 (no changes needed)

 -- Arthur de Jong <adejong@debian.org>  Sun, 31 Dec 2017 16:07:35 +0100

python-pskc (0.5-1) unstable; urgency=medium

  * New upstream release:
    - numerous compatibility improvements for reading PSKC files that do not
      follow the RFC 6030 schema exactly: specifically accept a number of old
      Internet Draft specifications that preceded RFC 6030 and support an
      ActivIdentity file format
    - split device information from key information (keep old API available)
      to allow multiple keys per device (this is not allowed by RFC 6030 but
      was allowed in older Internet Drafts)
    - accept MAC to be over plaintext in addition to ciphertext
    - fall back to using encryption key as MAC key
    - documentation improvements
  * Add file missing from upstream tarball
  * Upgrade to standards-version 3.9.8 (no changes needed)

 -- Arthur de Jong <adejong@debian.org>  Sat, 21 Jan 2017 22:26:25 +0100

python-pskc (0.4-2) unstable; urgency=medium

  * Add Breaks/Replaces to allow upgrades from older versions
    (closes: #820025)

 -- Arthur de Jong <adejong@debian.org>  Tue, 05 Apr 2016 22:13:26 +0200

python-pskc (0.4-1) unstable; urgency=medium

  * New upstream release:
    - add support for writing encrypted PSKC files (with either a pre-shared
      key or PBKDF2 password-based encryption)
    - extend may_use() policy checking function to check for unknown policy
      elements and key expiry
    - add a number of tests for existing vendor PSKC files and have full line
      coverage with tests
    - be more lenient in handling a number of XML files (e.g. automatically
      sanitise encryption algorithm URIs, ignore XML namespaces and support
      more spellings of some properties)
    - support reading password or key files in pskc2csv
    - support Python 3 in the pskc2csv script (thanks Mathias Laurin)
    - refactoring and clean-ups to be more easily extendible (thanks Mathias
      Laurin)
  * Use https in Vcs-Git field (thanks lintian)
  * Update package description
  * Add a python-pskc-doc package
  * Upgrade to standards-version 3.9.7 (no changes needed)

 -- Arthur de Jong <adejong@debian.org>  Mon, 28 Mar 2016 22:04:23 +0200

python-pskc (0.3-1) unstable; urgency=medium

  * New upstream release:
    - support writing unencrypted PSKC files
    - include a sample pskc2csv script in the source code
    - fix an issue with XML namespaces for PBKDF2 parameters
    - support Python 3
    - update documentation
  * Update debian/copyright
  * Update debhelper compatibility level to 9
  * Bump Standards-Version to 3.9.6, no changes needed
  * Switch to pybuild buildsystem
  * Also build a python3-pskc package for Python 3
  * Provide the pskc2csv.py example script

 -- Arthur de Jong <adejong@debian.org>  Thu, 08 Oct 2015 12:43:24 +0200

python-pskc (0.2-1) unstable; urgency=medium

  * New upstream release:
    - raise exceptions on parsing, decryption and other problems
    - support more encryption algorithms (AES128-CBC, AES192-CBC, AES256-CBC,
      TripleDES-CBC, KW-AES128, KW-AES192, KW-AES256 and KW-TripleDES) and be
      more lenient in accepting algorithm URIs
    - support all HMAC algorithms that Python's hashlib module has hash
      functions for (HMAC-MD5, HMAC-SHA1, HMAC-SHA224, HMAC-SHA256,
      HMAC-SHA384 and HMAC-SHA512)
    - support PRF attribute of PBKDF2 algorithm
  * Build and install Sphinx documentation.

 -- Arthur de Jong <adejong@debian.org>  Fri, 20 Jun 2014 14:50:59 +0200

python-pskc (0.1-1) unstable; urgency=medium

  * Initial release.

 -- Arthur de Jong <adejong@debian.org>  Fri, 23 May 2014 16:00:30 +0200
